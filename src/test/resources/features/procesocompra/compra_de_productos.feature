#language: es
Característica: Proceso de compra de productos
  Como Cliente
  Necesito realizar la compra de los productos que quiero
  Para que me los puedan enviar a mi direccion

  Esquema del escenario: Compra de productos exitosa
    Dado que he iniciado sesion sesion con mi "<usuario>" y "<contrasena>"
    Cuando adiciono productos al carrito
    Y realizo correctamente el proceso de compra de los productos
    Entonces observo un mensaje de que mi pedido llegara pronto

    Ejemplos:
      | usuario                 | contrasena   |
      | standard_user           | secret_sauce |
      | locked_out_user         | secret_sauce |
      | problem_user            | secret_sauce |
      | performance_glitch_user | secret_sauce |
