package com.sofka.actions;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;

public class AccionesPaginas {

    private final WebDriver webDriver;

    public AccionesPaginas(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    protected void desplazarHacia(WebElement webElement) {
        JavascriptExecutor jse = (JavascriptExecutor) webDriver;
        jse.executeScript("arguments[0].scrollIntoView();", webElement);
    }

    protected void digitarEn(WebElement webElement, String texto) {
        desplazarHacia(webElement);
        webElement.clear();
        webElement.sendKeys(texto);
    }

    protected void hacerClickEn(WebElement webElement) {
        desplazarHacia(webElement);
        webElement.click();
    }

    protected void seleccionarDe(WebElement webElement, String opcion) {
        Select desplegable = new Select(webElement);
        desplazarHacia(webElement);
        desplegable.selectByValue(opcion);
    }

    protected String obtenerTextoDe(WebElement webElement) {
        desplazarHacia(webElement);
        return webElement.getText();
    }

}
