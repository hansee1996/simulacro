#language: es
Característica: Iniciar sesion en el sitio web
  Como Cliente
  Necesito autenticarme en el sitio web
  Para poder acceder a los servicios que ofrecen

  Esquema del escenario: Autenticacion exitosa de usuario
    Cuando inicio sesion con mi "<usuario>" y "<contrasena>"
    Entonces observo que he ingresado de manera exitosa a la pagina web

    Ejemplos:
      | usuario                 | contrasena   |
      | standard_user           | secret_sauce |
      | locked_out_user         | secret_sauce |
      | problem_user            | secret_sauce |
      | performance_glitch_user | secret_sauce |
    
  Esquema del escenario: Autenticacion con contrasena incorrecta
    Cuando inicio sesion con mi "<usuario>" y una "<contrasena>" incorrecta
    Entonces observo un mensaje de error al intentar inicar sesion

    Ejemplos:
      | usuario                 | contrasena   |
      | standard_user           | invalid_sauce |
      | locked_out_user         | invalid_sauce |
      | problem_user            | invalid_sauce |
      | performance_glitch_user | invalid_sauce |

