package com.sofka.pages.setupdriver;

import com.sofka.pages.carrito.PaginaCarrito;
import com.sofka.pages.iniciosesion.PaginaInicioSesion;
import com.sofka.pages.inventario.PaginaInventarioProductos;
import com.sofka.pages.procesocompra.PaginaCompraFinalizada;
import com.sofka.pages.procesocompra.PaginaCompraPasoDos;
import com.sofka.pages.procesocompra.PaginaCompraPasoUno;
import org.openqa.selenium.WebDriver;

public class Navegador {
    private final WebDriver browser;

    public Navegador(WebDriver webDriver) {
        this.browser = webDriver;
    }

    public PaginaInicioSesion paginaInicioSesion() {
        return new PaginaInicioSesion(browser);
    }

    public PaginaInventarioProductos paginaInventarioProductos() {
        return new PaginaInventarioProductos(browser);
    }

    public PaginaCarrito paginaCarrito() {
        return new PaginaCarrito(browser);
    }

    public PaginaCompraPasoUno paginaCompraPasoUno() {
        return new PaginaCompraPasoUno(browser);
    }

    public PaginaCompraPasoDos paginaCompraPasoDos() {
        return new PaginaCompraPasoDos(browser);
    }

    public PaginaCompraFinalizada paginaCompraFinalizada() {
        return new PaginaCompraFinalizada(browser);
    }
}
