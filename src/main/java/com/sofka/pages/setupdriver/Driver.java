package com.sofka.pages.setupdriver;

import com.sofka.utils.ControladorNavegadorPaginas;
import io.github.bonigarcia.wdm.WebDriverManager;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.ThucydidesSystemProperty;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.util.EnvironmentVariables;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.util.Optional;

import static net.serenitybdd.core.environment.ConfiguredEnvironment.getConfiguration;

public class Driver {
    @Managed
    private WebDriver webDriver;
    private EnvironmentVariables environmentVariables;
    ControladorNavegadorPaginas controladorNavegadorPaginas = ControladorNavegadorPaginas.INSTANCE;

    private static final Logger LOGGER = Logger.getLogger(Driver.class);

    protected Navegador navegarEn() {
        if (!controladorNavegadorPaginas.estaActivo()) {
            inicializarNavegadorWeb();
            controladorNavegadorPaginas.verificarPaginaBase();
        }
        abrirUrlBase();
        return controladorNavegadorPaginas.obtenerPaginaBase();
    }

    private void inicializarNavegadorWeb() {
        configuracionNavegadorWeb(webDriver);
        controladorNavegadorPaginas.instanciarPaginaBase(webDriver);
    }

    private void configuracionNavegadorWeb(WebDriver browser) {
        creacionNavegadorWeb();
        browser.manage().deleteAllCookies();
    }

    private void creacionNavegadorWeb() {
        Optional<String> browserOptionalName = EnvironmentSpecificConfiguration.from(environmentVariables)
                .getOptionalProperty(ThucydidesSystemProperty.WEBDRIVER_DRIVER);
        String browserName = browserOptionalName.stream().findFirst().orElse("chrome");

        LOGGER.info("El navegador seleccionado es ".concat(browserName));

        switch (browserName) {
            case "chrome":
                WebDriverManager.chromedriver().setup();
                break;
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                break;
            case "safari":
                WebDriverManager.safaridriver().setup();
                break;
            case "edge":
                WebDriverManager.edgedriver().setup();
                break;
            default:
                LOGGER.info("No se pudo identificar el navegador a utilizar");
        }
    }

    private void abrirUrlBase() {
        LOGGER.info(webDriver.getCurrentUrl());

        if (webDriver.getCurrentUrl().contains("data:,") || webDriver.getCurrentUrl().isEmpty()) {
            webDriver.get(getConfiguration().getBaseUrl());
            LOGGER.info("AAAAAAAAAAAA");
        }
    }
}
