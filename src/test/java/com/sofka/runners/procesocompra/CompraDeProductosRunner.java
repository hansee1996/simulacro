package com.sofka.runners.procesocompra;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/procesocompra/compra_de_productos.feature",
        glue = {"com.sofka.definitions"},
        plugin = "html:target/cucumber-report/compra-productos/co_pr_cucumber.html"
)
public class CompraDeProductosRunner {

}

