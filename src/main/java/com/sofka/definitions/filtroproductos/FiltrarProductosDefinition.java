package com.sofka.definitions.filtroproductos;

import com.sofka.pages.setupdriver.Driver;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Entonces;
import org.assertj.core.api.Assertions;

import java.util.Comparator;

public class FiltrarProductosDefinition extends Driver {

    @Cuando("deseo filtrar los productos de menor a mayor precio")
    public void deseoFiltrarLosProductosDeMenorAMayorPrecio() {
        try {
            navegarEn()
                    .paginaInventarioProductos()
                    .filtrarPrecioMenorAMayor();
        } catch (Exception exception) {
            Assertions.fail("Problema seleccionando un modo de filtro", exception);
        }
    }

    @Entonces("el primer producto en la lista tiene el precio mas bajo")
    public void elPrimerProductoEnLaListaTieneElPrecioMasBajo() {
        double precioPrimerProducto =
                navegarEn()
                        .paginaInventarioProductos()
                        .obtenerPrecioPrimerProducto();

        double productoConMenorPrecio =
                navegarEn()
                        .paginaInventarioProductos()
                        .verPreciosDeLosProductos()
                        .stream().min(Comparator.naturalOrder()).orElseThrow();

        try {
            Assertions
                    .assertThat(precioPrimerProducto)
                    .isEqualTo(productoConMenorPrecio);
        } catch (Exception exception) {
            Assertions.fail("Error verificando el producto de menor precio", exception);
        }
    }
}
