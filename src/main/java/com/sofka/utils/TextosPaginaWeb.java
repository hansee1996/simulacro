package com.sofka.utils;

public class TextosPaginaWeb {

    private TextosPaginaWeb(){}
    public static final String MENSAJE_ERROR_AUTENTICACION_NO_VALIDA = "Username and password do not match any user in this service";
    public static final String MENSAJE_COMPRA_FINALIZADA = "CHECKOUT: COMPLETE!";
    public static final String MENSAJE_GRACIAS_POR_COMPRA = "THANK YOU FOR YOUR ORDER";
}
