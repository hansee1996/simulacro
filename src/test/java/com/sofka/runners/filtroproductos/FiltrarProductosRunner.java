package com.sofka.runners.filtroproductos;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/filtroproductos/filtrar_productos.feature",
        glue = {"com.sofka.definitions"},
        plugin = "html:target/cucumber-report/filtro-productos/fi_pr_cucumber.html"
)
public class FiltrarProductosRunner {

}

