package com.sofka.definitions.iniciosesion;

import com.sofka.pages.setupdriver.Driver;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Entonces;
import org.assertj.core.api.Assertions;

import static com.sofka.utils.TextosPaginaWeb.MENSAJE_ERROR_AUTENTICACION_NO_VALIDA;

public class IniciarSesionDefinition extends Driver {

    @Cuando("inicio sesion con mi {string} y {string}")
    public void inicioSesionConMiY(String usuario, String contrasena) {
        try {
            navegarEn()
                    .paginaInicioSesion()
                    .iniciarSesionCon(usuario, contrasena);
        } catch (Exception exception) {
            Assertions.fail("Se presento un fallo al completar el formulario de inicio de sesion", exception);
        }
    }

    @Entonces("observo que he ingresado de manera exitosa a la pagina web")
    public void observoQueHeIngresadoDeManeraExitosaALaPaginaWeb() {
        try {
            Assertions.assertThat(
                    navegarEn()
                            .paginaInventarioProductos()
                            .verCabecera().isDisplayed()).isTrue();
        } catch (Exception exception) {
            Assertions.fail("No se pudo iniciar sesion", exception);
        }
    }

    @Cuando("inicio sesion con mi {string} y una {string} incorrecta")
    public void inicioSesionConMiYUnaIncorrecta(String usuario, String contrasenaInvalida) {
        try {
            navegarEn()
                    .paginaInicioSesion()
                    .iniciarSesionCon(usuario, contrasenaInvalida);
        } catch (Exception exception) {
            Assertions.fail("Se presento un fallo al completar el formulario de inicio de sesion", exception);
        }
    }

    @Entonces("observo un mensaje de error al intentar inicar sesion")
    public void observoUnMensajeDeErrorAlIntentarInicarSesion() {
        try {
            Assertions.assertThat(
                    navegarEn()
                            .paginaInicioSesion()
                            .verMensajeError()).contains(MENSAJE_ERROR_AUTENTICACION_NO_VALIDA);
        } catch (Exception exception) {
            Assertions.fail("No se pudo iniciar sesion", exception);
        }
    }
}
