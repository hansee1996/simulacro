package com.sofka.pages.procesocompra;

import com.sofka.actions.AccionesPaginas;
import com.sofka.models.usersinfo.DatosUsuarioCompra;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.sofka.utils.ValoresAleatorios.datosParaCompra;

public class PaginaCompraPasoUno extends AccionesPaginas {
    public PaginaCompraPasoUno(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "first-name")
    private WebElement campoNombre;

    @FindBy(id = "last-name")
    private WebElement campoApellido;

    @FindBy(id = "postal-code")
    private WebElement campoCodigoPostal;

    @FindBy(id = "continue")
    private WebElement btnContinuar;

    public PaginaCompraPasoUno llenarInformacionObligatoria() {
        DatosUsuarioCompra usuario = datosParaCompra();

        digitarEn(campoNombre, usuario.getNombre());
        digitarEn(campoApellido, usuario.getApellido());
        digitarEn(campoCodigoPostal, usuario.getCodigoPostal());

        hacerClickEn(btnContinuar);
        return this;
    }
}
