package com.sofka.definitions.procesocompra;

import com.sofka.pages.setupdriver.Driver;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;

import static com.sofka.utils.TextosPaginaWeb.MENSAJE_COMPRA_FINALIZADA;
import static com.sofka.utils.TextosPaginaWeb.MENSAJE_GRACIAS_POR_COMPRA;

public class CompraDeProductosDefinition extends Driver {

    @Dado("que he iniciado sesion sesion con mi {string} y {string}")
    public void queHeIniciadoSesionSesionConMiY(String usuario, String contrasena) {
        try {
            navegarEn()
                    .paginaInicioSesion()
                    .iniciarSesionCon(usuario, contrasena);
        } catch (Exception exception) {
            Assertions.fail("Se presento un fallo al completar el formulario de inicio de sesion");
        }
    }

    @Cuando("adiciono productos al carrito")
    public void adicionoProductosAlCarrito() {
        try {
            navegarEn()
                    .paginaInventarioProductos()
                    .agregarProductos(3)
                    .ingresarAlCarrito();
        } catch (Exception exception) {
            Assertions.fail("Se presento un problema agregando productos al carrito", exception);
        }
    }

    @Cuando("realizo correctamente el proceso de compra de los productos")
    public void realizoCorrectamenteElProcesoDeCompraDeLosProductos() {
        try {
            navegarEn().paginaCarrito()
                    .confirmarProductosCarrito();

            navegarEn().paginaCompraPasoUno()
                    .llenarInformacionObligatoria();

            navegarEn().paginaCompraPasoDos()
                    .finalizarCompra();
        } catch (Exception exception) {
            Assertions.fail("Se presento un problema en el proceso de compra de productos", exception);
        }
    }

    @Entonces("observo un mensaje de que mi pedido llegara pronto")
    public void observoUnMensajeDeQueMiPedidoLlegaraPronto() {
        try {
            SoftAssertions softly = new SoftAssertions();
            softly
                    .assertThat(
                            navegarEn()
                                    .paginaCompraFinalizada().verMensajeCompraCompletada())
                    .isEqualTo(MENSAJE_COMPRA_FINALIZADA);
            softly
                    .assertThat(
                            navegarEn()
                                    .paginaCompraFinalizada().verMensajeGraciasPorLaCompra())
                    .isEqualTo(MENSAJE_GRACIAS_POR_COMPRA);
            softly.assertAll();
        } catch (Exception exception) {
            Assertions.fail("Problema en el mensaje de la compra finalizada", exception);
        }
    }

}
