package com.sofka.pages.procesocompra;

import com.sofka.actions.AccionesPaginas;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PaginaCompraPasoDos extends AccionesPaginas {

    public PaginaCompraPasoDos(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "finish")
    private WebElement btnFinalizarCompra;

    public PaginaCompraPasoDos finalizarCompra(){
        hacerClickEn(btnFinalizarCompra);
        return this;
    }
}
