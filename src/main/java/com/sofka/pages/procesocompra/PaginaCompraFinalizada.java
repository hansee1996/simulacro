package com.sofka.pages.procesocompra;

import com.sofka.actions.AccionesPaginas;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PaginaCompraFinalizada extends AccionesPaginas {

    public PaginaCompraFinalizada(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//div[@class='header_secondary_container']/span")
    private WebElement msgCompraCompletada;

    @FindBy(xpath = "//div[@id='checkout_complete_container']/h2")
    private WebElement msgGracias;

    public WebElement getMsgGracias() {
        return msgGracias;
    }

    public void setMsgGracias(WebElement msgGracias) {
        this.msgGracias = msgGracias;
    }

    public String verMensajeCompraCompletada(){
        return obtenerTextoDe(msgCompraCompletada);
    }

    public String verMensajeGraciasPorLaCompra(){
        return obtenerTextoDe(msgGracias);
    }
}
