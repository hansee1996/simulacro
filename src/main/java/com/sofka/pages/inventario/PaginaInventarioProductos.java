package com.sofka.pages.inventario;

import com.sofka.actions.AccionesPaginas;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.stream.Collectors;

public class PaginaInventarioProductos extends AccionesPaginas {

    public PaginaInventarioProductos(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//div[@id='shopping_cart_container']/a")
    private WebElement btnCarrito;

    @FindBy(id = "header_container")
    private WebElement cabeceraInventarios;

    @FindBy(className = "product_sort_container")
    private WebElement desplegableParaFiltrar;

    @FindAll({@FindBy(css = ".btn.btn_primary.btn_small.btn_inventory")})
    private List<WebElement> btnsAgregarProducto;

    @FindAll({@FindBy(className = "inventory_item_price")})
    private List<WebElement> preciosProductos;


    public WebElement verCabecera() {
        return cabeceraInventarios;
    }

    public PaginaInventarioProductos ingresarAlCarrito() throws InterruptedException {
        hacerClickEn(btnCarrito);
        return this;
    }

    public PaginaInventarioProductos filtrarPrecioMenorAMayor() {
        seleccionarDe(desplegableParaFiltrar, "lohi");
        return this;
    }

    public PaginaInventarioProductos agregarProductos(int cantidad) {
        if (cantidad > btnsAgregarProducto.size() && btnsAgregarProducto.get(0).isDisplayed()) {
            throw new IllegalArgumentException("No existe esa cantidad de productos en la pagina");
        }

        btnsAgregarProducto.stream().limit(cantidad).forEach(
                this::hacerClickEn
        );
        return this;
    }

    public double obtenerPrecioPrimerProducto() {
        String precioTexto = obtenerTextoDe(preciosProductos.get(0)).substring(1);
        return Double.parseDouble(precioTexto);
    }

    public List<Double> verPreciosDeLosProductos() {
        return preciosProductos.stream().map(
                webElement ->
                        Double.parseDouble(webElement.getText().substring(1))
        ).collect(Collectors.toList());
    }
}
