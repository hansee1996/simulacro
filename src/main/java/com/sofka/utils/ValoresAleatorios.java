package com.sofka.utils;

import com.github.javafaker.Faker;
import com.sofka.models.usersinfo.DatosUsuarioCompra;

public class ValoresAleatorios {

    public static final Faker faker = new Faker();

    private ValoresAleatorios() {
    }

    public static DatosUsuarioCompra datosParaCompra() {
        return new DatosUsuarioCompra(faker.name().name(), faker.name().lastName(), faker.address().zipCode());
    }
}
