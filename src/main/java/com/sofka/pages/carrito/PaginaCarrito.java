package com.sofka.pages.carrito;

import com.sofka.actions.AccionesPaginas;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PaginaCarrito extends AccionesPaginas {

    public PaginaCarrito(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "checkout")
    private WebElement btnVerificarCarrito;

    public void confirmarProductosCarrito()  {
        hacerClickEn(btnVerificarCarrito);
    }
}
