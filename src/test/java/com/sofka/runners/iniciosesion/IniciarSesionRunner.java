package com.sofka.runners.iniciosesion;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/iniciosesion/iniciar_sesion.feature",
        glue = {"com.sofka.definitions"},
        plugin = "html:target/cucumber-report/iniciar-sesion/in_se_cucumber.html"
)
public class IniciarSesionRunner {

}
