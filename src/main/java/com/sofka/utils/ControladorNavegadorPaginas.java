package com.sofka.utils;

import com.sofka.pages.setupdriver.Navegador;
import org.openqa.selenium.WebDriver;

public enum ControladorNavegadorPaginas {
    INSTANCE;
    private Navegador navegador;
    private boolean estaActivo = false;

    public Navegador obtenerPaginaBase() {
        return navegador;
    }

    public boolean estaActivo() {
        return estaActivo;
    }

    public void instanciarPaginaBase(WebDriver webDriver) {
        this.navegador = new Navegador(webDriver);
    }

    public void verificarPaginaBase() {
        this.estaActivo = !estaActivo;
    }

}
