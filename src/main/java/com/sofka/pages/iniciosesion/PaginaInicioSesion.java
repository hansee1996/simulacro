package com.sofka.pages.iniciosesion;

import com.sofka.actions.AccionesPaginas;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PaginaInicioSesion extends AccionesPaginas {

    public PaginaInicioSesion(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "user-name")
    private WebElement campoUsuario;

    @FindBy(id = "password")
    private WebElement campoContrasena;

    @FindBy(id = "login-button")
    private WebElement confirmarInicioSesion;

    @FindBy(xpath = "//h3[@data-test='error']")
    private WebElement msgErrorInicioSesion;

    public PaginaInicioSesion iniciarSesionCon(String usuario, String contrasena) {
        digitarEn(campoUsuario, usuario);
        digitarEn(campoContrasena, contrasena);
        hacerClickEn(confirmarInicioSesion);
        return this;
    }

    public String verMensajeError(){
        return obtenerTextoDe(msgErrorInicioSesion);
    }
}
