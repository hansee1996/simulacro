#language: es
Característica: Filtro para la lista de productos ofertados
  Como Cliente
  Necesito ordenar los productos que se ofrecen en la pagina
  Para facilitar la busqueda de los productos que deseo

  Esquema del escenario: Filtrar productos por precio
    Cuando inicio sesion con mi "<usuario>" y "<contrasena>"
    Y deseo filtrar los productos de menor a mayor precio
    Entonces el primer producto en la lista tiene el precio mas bajo


    Ejemplos:
      | usuario                 | contrasena   |
      | standard_user           | secret_sauce |
      | locked_out_user         | secret_sauce |
      | problem_user            | secret_sauce |
      | performance_glitch_user | secret_sauce |
